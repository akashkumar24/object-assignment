function values(obj) {
  const result = [];

  for (const key in obj) {
    const value = obj[key];
    if (typeof value !== 'function') {
      result.push(value);
    }
  }

  return result;
}

module.exports = values;

