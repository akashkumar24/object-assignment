function defaults(obj, defaultProps) {
  if (typeof defaultProps !== 'object' || defaultProps === null) {
    return obj;
  }

  for (let key in defaultProps) {
    if (defaultProps.hasOwnProperty(key) && !obj.hasOwnProperty(key)) {
      obj[key] = defaultProps[key];
    }
  }

  return obj;
}

module.exports = defaults;

