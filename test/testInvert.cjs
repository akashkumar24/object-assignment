const invert = require('../invert.cjs');

// Test object
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


// Test the values function
const result = invert(testObject);
console.log(result);
