const mapObject = require('../mapObject.cjs');

function double(value) {
  return value * 2;
}

const inputObject = { a: 1, b: 2, c: 3 };

console.log(mapObject(inputObject, double)); 

