function mapObject(obj, cb) {
  const result = {};
  for (const key in obj) {
    result[key] = cb(obj[key], key, obj);
  }
  return result;
}

module.exports = mapObject;

